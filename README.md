# Diving In Python

This one includes final tasks (week 5 and 6) for "Diving In Python" [course](https://www.coursera.org/learn/diving-in-python) at Coursera. I've done these tasks during July 2019. I'll add the descriptions for each task a little bit later.

To cut a long story short, students there had to create a client-server application that would implement sending and storing some metrics.
