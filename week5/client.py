import socket
import time


class ClientError(Exception):
	pass
	

class Client:
	def __init__(self, host, port, timeout=None):
		self.host = host
		self.port = port
		self.timeout = timeout
		try:
			self.conn = socket.create_connection((self.host, self.port), self.timeout)
		except socket.error:
			print('socket error')

	def put(self, key, value, timestamp=None):
		timestamp = timestamp or str(int(time.time()))
		try:
			self.conn.send(b'put ' + key.encode('utf-8') + b' ' + str(value).encode('utf-8') + b' ' +
					str(timestamp).encode('utf-8') + b'\n')
			response = self.conn.recv(1024).decode('utf-8')
			if response != 'ok\n\n':
				raise ClientError('ClientError: send error')
		except ClientError:
			print(response)

	def get(self, key):
		response_dict = {}
		try:
			self.conn.send(b'get ' + str(key).encode('utf-8'))
			response = self.conn.recv(1024).decode('utf-8')
			if response[:4] != 'ok\n':
				raise ClientError('ClientError: get error')
		except ClientError:
			print(response)	
		else:
			if response.endswith('\n\n'):
				end_resp = True			

			data = response[4:].split('\n')
			response = data[-1]
			for i in data:
				data_list = i.split()
				if data_list[0] in response_dict:
					response_dict[data_list[0]].append((int(data_list[2]), float(data_list[1])))
				else:
					response_dict[data_list[0]] = [(int(data_list[2]), float(data_list[1]))]
					
			while not end_resp:
				response += self.conn.recv(1024).decode('utf-8')
				data = response.split('\n')
				response = data[-1]
				del(data[-1])
				for i in data:
					data_list = i.split()
					if data_list[0] in response_dict:
						response_dict[data_list[0]].append((int(data_list[2]), float(data_list[1])))
					else:
						response_dict[data_list[0]] = [(int(data_list[2]), float(data_list[1]))]

				end_resp = True
