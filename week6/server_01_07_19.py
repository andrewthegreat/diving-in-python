import asyncio


def run_server(host, port):
	"""runs the server"""
	loop = asyncio.get_event_loop()
	coro = loop.create_server(ClientServerProtocol, host, port)
	server = loop.run_until_complete(coro)
	try:
		loop.run_forever()
	except KeyboardInterrupt:
		pass
	server.close()
	loop.run_until_complete(server.wait_closed())
	loop.close()

class ClientServerProtocol(asyncio.Protocol):
	def encoding(self, request):
		global data_dict
		"""encodes the client's request; saves metrics given"""
		if request[1] not in data_dict:
			data_dict[request[1]] = {}
		data_dict[request[1]][int(request[3])] = float(request[2])
		return 'ok\n\n'

	def decoding(self, request):
		"""makes and encodes the server's response"""
		global data_dict
		response = 'ok\n'
		if request[1] == '*':
			for key1, value1 in data_dict.items():
				for key, value in value1.items():
					response += key1 + ' ' + str(value) + ' ' + str(key) + '\n'
		elif request[1] in data_dict:
			for key, value in data_dict[request[1]].items():
				response += request[1] + ' ' + str(value) + ' ' + str(key) + '\n'
		return response + '\n'
				
	def process_data(self, request):
		self.request = request
		data = self.request.split()
		if data[0] == 'put' and len(data) == 4 and data[3].isdigit():
			return self.encoding(data)
		elif data[0] == 'get' and len(data) == 2:
			return self.decoding(data)
		else:
			return 'error\nwrong command\n\n'

	def connection_made(self, transport):
		self.transport = transport

	def data_received(self, request):
		request = request.decode().rstrip('\n')
		self.transport.write(self.process_data(request).encode())


if __name__ == "__main__":
	data_dict = {}
	HOST = "127.0.0.1"
	PORT = 8888
	run_server(HOST, PORT)
